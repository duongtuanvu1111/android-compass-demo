package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.Languages
import com.apero.compass.domain.repository.LanguageRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations


class GetLanguageUsecaseTest {

    @Mock
    lateinit var languageRepository: LanguageRepository

    lateinit var getLanguageUseCase: GetLanguageUseCase

    @Before
    fun setup() {
        MockitoAnnotations.initMocks(this)
        getLanguageUseCase = GetLanguageUseCase(languageRepository)
    }

    @Test
    fun test_GetLanguages_Pass() {
        Mockito.`when`(languageRepository.getLanguages()).thenReturn(
            Languages(Languages.prepareLanguageList())
        )
        runBlocking {
            val a = getLanguageUseCase.invoke(Unit).drop(1).first()
            assert(a is State.DataState && a.data.languages.size == 4)
        }

    }

    @Test
    fun test_GetLanguages_Fail() {
        Mockito.`when`(languageRepository.getLanguages()).thenReturn(
            Languages(emptyList())
        )
        runBlocking {
            val a = getLanguageUseCase.invoke(Unit).drop(1).first()
            assert(a is State.ErrorState)
        }

    }


}