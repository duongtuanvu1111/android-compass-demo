package com.apero.compass.presentation.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.apero.compass.R
import com.apero.compass.databinding.DialogChooseCompassTypeBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ChooseCompassTypeDialog : BottomSheetDialogFragment(), View.OnClickListener {

    lateinit var binding: DialogChooseCompassTypeBinding
    var listener: ChooseCompassTypeListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_choose_compass_type,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btFengshuiCompass.setOnClickListener(this)
        binding.btMapCompass.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        dismiss()
        when (p0?.id) {
            R.id.btFengshuiCompass -> listener?.onFengshuiCompassSelected()
            R.id.btMapCompass -> listener?.onMapCompassSelected()
        }

    }

}

interface ChooseCompassTypeListener {
    fun onFengshuiCompassSelected()
    fun onMapCompassSelected()
}