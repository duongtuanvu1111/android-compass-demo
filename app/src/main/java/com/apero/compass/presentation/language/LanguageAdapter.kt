package com.apero.compass.presentation.language

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.apero.compass.R
import com.apero.compass.data.entity.Language
import com.apero.compass.data.entity.Languages
import com.apero.compass.databinding.ItemLanguageBinding

class LanguageAdapter(val languages: Languages, val onLanguageSelected: (Language) -> Unit) :
    RecyclerView.Adapter<LanguageAdapter.LanguageViewHolder>() {

    class LanguageViewHolder(val binding: ItemLanguageBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = LanguageViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_language,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: LanguageViewHolder, position: Int) {
        holder.binding.language = languages.languages[position]
        holder.binding.root.setOnClickListener {
            onLanguageSelected(languages.languages[position])
        }
        holder.binding.executePendingBindings()
    }

    override fun getItemCount() = languages.languages.size
}