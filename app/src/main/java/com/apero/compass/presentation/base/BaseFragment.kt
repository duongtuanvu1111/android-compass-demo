package com.apero.compass.presentation.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.Nullable
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import com.apero.compass.data.local.MultipleLanguageHelper
import com.apero.compass.utils.extension.displayKeyboard
import com.apero.compass.utils.log.Logger
import javax.inject.Inject

abstract class BaseFragment<Binding, BViewModel> :
    Fragment() where Binding : ViewDataBinding, BViewModel : ViewModel {

    protected lateinit var mDataBinding: Binding
    protected abstract val mViewModel: BViewModel

    @Inject lateinit var multipleLanguageHelper: MultipleLanguageHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Logger.d("OnCreate ${tag}")
        subscribeData()

    }

    @Nullable
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        if (!::mDataBinding.isInitialized) {
            initDataBinding(inflater, container)
            initView()

        }
        return mDataBinding.root;
    }

    private fun initDataBinding(inflater: LayoutInflater, container: ViewGroup?) {
        mDataBinding = DataBindingUtil.inflate(inflater, layoutId(), container, false)
        mDataBinding.lifecycleOwner = this
    }

    override fun onPause() {
        super.onPause()
        activity?.displayKeyboard(false)
    }


    protected var isShowDialog = true

    protected abstract fun layoutId(): Int
    protected abstract fun initView()
    protected abstract fun subscribeData()
}