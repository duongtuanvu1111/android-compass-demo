package com.apero.compass.presentation.map.withcompass

import com.apero.compass.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ViewMapWithCompassViewModel @Inject constructor(): BaseViewModel() {
}