package com.apero.compass.presentation.home

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Bundle
import androidx.activity.result.contract.ActivityResultContracts
import com.apero.compass.R
import com.apero.compass.presentation.base.BaseActivity
import com.apero.compass.utils.BroadcastUtils
import com.apero.compass.utils.DialogUtils
import com.apero.compass.utils.GpsUtils
import dagger.hilt.android.AndroidEntryPoint
import java.lang.String
import kotlin.Function1
import kotlin.Unit


@AndroidEntryPoint
class HomeActivity : BaseActivity() {
    val broadcastPermission = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        if (DialogUtils.INSTANCE.isLocationPermissionGranted(this)) {
            handlerGrantedPermissionLocation()
        } else {
            val locationPermissionRequest = registerForActivityResult(
                ActivityResultContracts.RequestMultiplePermissions()
            ) {
                run {
                    if (DialogUtils.INSTANCE.checkPmsIsGranted(
                            this,
                            "android.permission.ACCESS_COARSE_LOCATION"
                        ) && DialogUtils.INSTANCE.checkPmsIsGranted(
                            this,
                            "android.permission.ACCESS_FINE_LOCATION"
                        )
                    ) {
                        handlerGrantedPermissionLocation();
                    }
                }

            }
            locationPermissionRequest.launch(
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                )
            )
        }

    }

    override fun onResume() {
        super.onResume()
        if (DialogUtils.INSTANCE.checkPmsIsGranted(
                this,
                "android.permission.ACCESS_COARSE_LOCATION"
            ) && DialogUtils.INSTANCE.checkPmsIsGranted(
                this,
                "android.permission.ACCESS_FINE_LOCATION"
            )
        ) {
            handlerGrantedPermissionLocation();
        }
    }

    private fun handlerGrantedPermissionLocation() {
        GpsUtils(this).registerUpdateLocationListener(true,
            object : Function1<Location, Unit> {
                override fun invoke(location: Location) {
                    BroadcastUtils.INSTANCE.notifyLocationChange(
                        this@HomeActivity,
                        String.valueOf(location.latitude),
                        String.valueOf(location.longitude)
                    )

                }

            }
        )

    }
}