package com.apero.compass.presentation.policy

import android.view.View
import androidx.core.text.HtmlCompat
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.apero.compass.R
import com.apero.compass.data.local.MultipleLanguageHelper
import com.apero.compass.databinding.FragmentPolicyBinding
import com.apero.compass.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class PolicyFragment : BaseFragment<FragmentPolicyBinding, PolicyViewModel>(),
    View.OnClickListener {


    override val mViewModel: PolicyViewModel by viewModels()

    override fun layoutId(): Int = R.layout.fragment_policy

    override fun initView() {
        with(mDataBinding) {
            ivBack.setOnClickListener(this@PolicyFragment)

            lifecycleScope.launch(Dispatchers.Main) {
                val selectedLanguage = multipleLanguageHelper.getSelectedLanguage()
                tvTerm.setText(
                    multipleLanguageHelper.getString(
                        R.string.term_content,
                        selectedLanguage
                    )
                )
            }

        }
    }

    override fun subscribeData() {

    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.ivBack -> findNavController().navigateUp()
        }
    }

}