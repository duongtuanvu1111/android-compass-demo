package com.apero.compass.presentation.home

import androidx.lifecycle.viewModelScope
import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.domain.usecase.GetSelectedBackgroundThemeUseCase
import com.apero.compass.domain.usecase.GetSelectedCompassThemeUseCase
import com.apero.compass.presentation.State
import com.apero.compass.presentation.base.BaseLiveData
import com.apero.compass.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val getSelectedBackgroundThemeUseCase: GetSelectedBackgroundThemeUseCase,
    private val getSelectedCompassThemeUseCase: GetSelectedCompassThemeUseCase
) :
    BaseViewModel() {

    val selectedCompassTheme = BaseLiveData<CompassTheme>()
    val selectedBackgroundTheme = BaseLiveData<BackgroundTheme>()

    fun isDefaultCompassTheme(compassTheme: CompassTheme) : Boolean {
        return compassTheme.id == null
    }

    fun getSelectedCompassTheme() = viewModelScope.launch(Dispatchers.IO) {
        getSelectedCompassThemeUseCase.invoke(Unit).collect {
            when (it) {
                is State.DataState -> {
                    it.data?.let { compassTheme ->
                        selectedCompassTheme.postValue(compassTheme)
                    }
                }
            }
        }
    }

    fun getSelectedBackgroundTheme() = viewModelScope.launch(Dispatchers.IO) {
        getSelectedBackgroundThemeUseCase.invoke(Unit).collect {
            when (it) {
                is State.DataState -> {
                    it.data?.let { backgroundTheme ->
                        selectedBackgroundTheme.postValue(backgroundTheme)
                    }
                }
            }
        }
    }

}