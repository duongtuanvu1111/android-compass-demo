package com.apero.compass.presentation.map

import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.apero.compass.R
import com.apero.compass.databinding.FragmentMapBinding
import com.apero.compass.presentation.base.BaseFragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ViewMapFragment: BaseFragment<FragmentMapBinding, ViewMapViewModel>(), View.OnClickListener, OnMapReadyCallback {

    private lateinit var mMap: GoogleMap

    override val mViewModel: ViewMapViewModel by viewModels()

    override fun layoutId(): Int = R.layout.fragment_map

    override fun initView() {
        (childFragmentManager.findFragmentById(R.id.map) as? SupportMapFragment)?.getMapAsync(this)

        with(mDataBinding) {
            ivBack.setOnClickListener(this@ViewMapFragment)
            ivExport.setOnClickListener(this@ViewMapFragment)
        }
    }

    override fun subscribeData() {

    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        val location = LatLng(21.030653, 105.847130)
        mMap.addMarker(
            MarkerOptions()
            .position(location)
            .title("Marker in Hanoi"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(location))
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.ivBack -> findNavController().navigateUp()
        }
    }
}