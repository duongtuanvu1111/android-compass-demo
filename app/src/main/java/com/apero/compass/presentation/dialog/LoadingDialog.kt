package com.apero.compass.presentation.dialog

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.apero.compass.R
import java.lang.Exception

class LoadingDialog : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_loading, container, false)
    }

    fun hideDialog() {
        try {
            dismissAllowingStateLoss()
        }catch (e: Exception) {

        }
    }

    fun showDialog(fragmentManager: FragmentManager) {
        try {
            if(isAdded) {
                return
            }
            show(fragmentManager, javaClass.canonicalName)
        }catch (e: Exception) {

        }
    }

    override fun onStart() {
        super.onStart()
        if (activity == null) {
            return
        }
        val size = requireActivity().resources.getDimensionPixelSize(R.dimen._70sdp)
        val dialog = dialog
        dialog?.setCancelable(false)
        if (dialog != null) {
            dialog.window!!.setLayout(
                size,
                size
            )
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        }
    }
}