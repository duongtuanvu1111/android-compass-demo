package com.apero.compass.presentation.theme

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.apero.compass.R
import com.apero.compass.databinding.FragmentListBackgroundThemeBinding
import com.apero.compass.presentation.base.BaseFragment
import com.apero.compass.presentation.dialog.LoadingDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectBackgroundThemeFragment :
    BaseFragment<FragmentListBackgroundThemeBinding, SelectThemeViewModel>() {

    val loading: LoadingDialog by lazy {
        LoadingDialog()
    }

    override val mViewModel: SelectThemeViewModel by viewModels()

    override fun layoutId() = R.layout.fragment_list_background_theme

    override fun initView() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.getListBackgroundTheme()
    }

    override fun subscribeData() {
        mViewModel.loading.observe(this) {
            when (it) {
                true -> loading.showDialog(childFragmentManager)
                else -> loading.hideDialog()
            }
        }

        mViewModel.listBackgroundTheme.observe(this) {
            val adapter = BackgroundThemeAdapter(it) {
                mViewModel.setBackgroundThemeSelected(it)
            }
            mDataBinding.rvTheme.adapter = adapter
        }

        mViewModel.selectBackgroundThemeResult.observe(this) {
            if (it) {
                findNavController().navigateUp()
            }
        }
    }
}