package com.apero.compass.presentation.theme

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.apero.compass.R
import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.databinding.ItemBackgroundThemeBinding
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CenterCrop
import com.bumptech.glide.load.resource.bitmap.RoundedCorners
import com.bumptech.glide.request.RequestOptions

class BackgroundThemeAdapter(
    val themes: List<BackgroundTheme>,
    val onThemeSelected: (BackgroundTheme) -> Unit
) :
    RecyclerView.Adapter<BackgroundThemeAdapter.BackgroundThemeViewHolder>() {

    class BackgroundThemeViewHolder(val binding: ItemBackgroundThemeBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = BackgroundThemeViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_background_theme,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: BackgroundThemeViewHolder, position: Int) {
        holder.binding.theme = themes[position]
        holder.binding.root.setOnClickListener {
            onThemeSelected(themes[position])
        }
        holder.binding.executePendingBindings()
    }

    override fun getItemCount() = themes.size
}

@BindingAdapter("loadBackgroundThemeThumbnail")
fun loadBackgroundThemeThumbnail(imageView: ImageView, theme: BackgroundTheme) {
    Glide.with(imageView.context).load(theme.thumbnail ?: "").apply(
        RequestOptions().transform(CenterCrop(), RoundedCorners(10))
    ).into(imageView)
}