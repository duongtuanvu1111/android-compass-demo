package com.apero.compass.presentation.map

import com.apero.compass.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ViewMapViewModel @Inject constructor(): BaseViewModel() {
}