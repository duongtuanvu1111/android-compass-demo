package com.apero.compass.presentation.onboarding

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.viewpager.widget.ViewPager
import com.apero.compass.R
import com.apero.compass.databinding.FragmentOnboardingBinding
import com.apero.compass.presentation.base.BaseFragment
import com.apero.compass.presentation.home.HomeActivity
import com.apero.compass.presentation.onboarding.adapter.TutorialAdapter
import com.apero.compass.presentation.onboarding.fragment.OnBoarding1Fragment
import com.apero.compass.presentation.onboarding.fragment.OnBoarding2Fragment
import com.apero.compass.presentation.onboarding.fragment.OnBoarding3Fragment
import com.apero.compass.utils.extension.beVisible
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OnBoardingFragment : BaseFragment<FragmentOnboardingBinding, OnBoardingViewModel>(),
    View.OnClickListener, ViewPager.OnPageChangeListener {

    private val fragmentList = ArrayList<Fragment>()

    override val mViewModel: OnBoardingViewModel by viewModels()

    override fun layoutId(): Int = R.layout.fragment_onboarding

    override fun initView() {
        fragmentList.add(OnBoarding1Fragment())
        fragmentList.add(OnBoarding2Fragment())
        fragmentList.add(OnBoarding3Fragment())
        val tutorialAdapter = TutorialAdapter(parentFragmentManager, fragmentList)

        mDataBinding.apply {
            viewPager.adapter = tutorialAdapter
            viewPager.addOnPageChangeListener(this@OnBoardingFragment)
            viewPager.offscreenPageLimit = 0
            btnNext.setOnClickListener(this@OnBoardingFragment)
        }
    }

    override fun subscribeData() {
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.apply {
            actionToHome.observe(viewLifecycleOwner) {
                startActivity(Intent(requireContext(), HomeActivity::class.java))
                requireActivity().finish()
            }

            actionShowOnBoarding.observe(viewLifecycleOwner) {
                mDataBinding.vOnBoarding.beVisible()
            }
        }
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        mDataBinding
        mDataBinding.apply {
            btnNext.text =
                if (position == fragmentList.size - 1) getString(R.string.button_start_now) else getString(
                    R.string.button_continue
                )
            indicator1.setImageResource(getBackground(0, position))
            indicator2.setImageResource(getBackground(1, position))
            indicator3.setImageResource(getBackground(2, position))
        }
    }

    private fun getBackground(indicatorPosition: Int, viewpagerPosition: Int): Int {
        return if (indicatorPosition == viewpagerPosition) R.drawable.ic_indicator_rectangle
        else R.drawable.ic_indicator_ellipse
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onClick(view: View?) {
        when (view?.id) {
            R.id.btnNext -> {
                mDataBinding.apply {
                    if (viewPager.currentItem < fragmentList.size - 1) {
                        viewPager.setCurrentItem(viewPager.currentItem + 1, true)
                    } else {
                        mViewModel.saveStateOnBoarding()
                        startActivity(Intent(requireContext(), HomeActivity::class.java))
                        activity?.finish()
                    }
                }
            }
        }
    }
}