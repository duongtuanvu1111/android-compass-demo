package com.apero.compass.presentation.map.location

import android.app.ActivityManager
import android.content.Context
import android.content.Intent
import androidx.core.content.ContextCompat
import kotlin.jvm.internal.Intrinsics


class LocationServiceManager {
    companion object {
        val INSTANCE = LocationServiceManager()
    }

    @SuppressWarnings("deprecation")
    fun isLocationServiceNotRunning(context: Context): Boolean {
        val systemService: ActivityManager =
            context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        for (runningServiceInfo in systemService.getRunningServices(Int.MAX_VALUE)) {
            val name = LocationService::class.java.name
            val componentName = runningServiceInfo.service
            if (Intrinsics.areEqual(name, componentName.className)) {
                return false
            }
        }
        return true
    }

    fun startLocationService(context: Context) {
        if (isLocationServiceNotRunning(context)) {
            ContextCompat.startForegroundService(
                context,
                Intent(context, LocationService::class.java)
            )
        }
    }
    fun stopLocationService(context: Context) {
        if (!isLocationServiceNotRunning(context)) {
            context.stopService(Intent(context, LocationService::class.java))
        }
    }

}