package com.apero.compass.presentation.map.activity

import android.os.Bundle
import com.apero.compass.R
import com.apero.compass.presentation.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MapCompassActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compass_map)
    }


}