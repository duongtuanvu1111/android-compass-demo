package com.apero.compass.presentation.language

import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.annotation.RequiresApi
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.apero.compass.R
import com.apero.compass.databinding.FragmentLanguageBinding
import com.apero.compass.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint
import java.util.*

@AndroidEntryPoint
class LanguageFragment : BaseFragment<FragmentLanguageBinding, LanguageViewModel>(),
    View.OnClickListener {

    override val mViewModel: LanguageViewModel by viewModels()

    override fun layoutId() = R.layout.fragment_language

    override fun initView() {
        with(mDataBinding) {
            ivBack.setOnClickListener(this@LanguageFragment)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.getLanguages()
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun subscribeData() {
        mViewModel.languages.observe(this) {
            val adapter = LanguageAdapter(it) {
                mViewModel.setLanguage(language = it)

                when(it.code) {
                }
            }
            mDataBinding.rvLanguage.adapter = adapter
        }
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.ivBack -> findNavController().navigateUp()
        }
    }
}