package com.apero.compass.presentation.policy

import com.apero.compass.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PolicyViewModel @Inject constructor(): BaseViewModel() {

}