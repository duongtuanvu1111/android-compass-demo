package com.apero.compass.presentation.home

import android.content.*
import android.view.View
import android.view.animation.RotateAnimation
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.apero.compass.R
import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.data.entity.Language
import com.apero.compass.data.entity.Location
import com.apero.compass.data.local.FileUtils
import com.apero.compass.data.local.ImageType
import com.apero.compass.databinding.FragmentHomeBinding
import com.apero.compass.presentation.base.BaseFragment
import com.apero.compass.presentation.dialog.ChooseCompassTypeDialog
import com.apero.compass.presentation.dialog.ChooseCompassTypeListener
import com.apero.compass.utils.BroadcastUtils
import com.apero.compass.utils.LocationUtils
import com.apero.compass.utils.format.SOTWFormatter
import com.apero.compass.utils.log.Logger
import com.apero.compass.utils.sensor.CompassManager
import com.apero.compass.utils.sensor.MagneticSensor
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding, HomeViewModel>(), View.OnClickListener,
    CompassManager.CompassListener, (Double) -> Unit, ChooseCompassTypeListener {
    private var currentAzimuth: Float = 0.0f
    private lateinit var selectedLanguage: Language
    var mCompassManager: CompassManager? = null
    var mMagneticSensor: MagneticSensor? = null
    var mSOTWFormatter: SOTWFormatter? = null
    var location = Location("0", "0")
    var locationUpdateReceiver = object : BroadcastReceiver() {
        override fun onReceive(p0: Context?, intent: Intent?) {
            val lat: String? = intent?.getStringExtra("latitude")
            val lng: String? = intent?.getStringExtra("longitude")
            var latnumber = lat?.toDouble()
            var lngnumber = lng?.toDouble()
            location = Location(lat.toString(), lng.toString())
            if (lat != null) {
                mDataBinding.tvLat.text = LocationUtils.INSTANCE.convertLatToString(latnumber!!)
            }
            if (lng != null) {
                mDataBinding.tvLng.text = LocationUtils.INSTANCE.convertLngToString(lngnumber!!)
            }
            mDataBinding.tvAddress.text =
                LocationUtils.INSTANCE.getAddressWithLatLng(requireContext(),
                    latnumber!!, lngnumber!!, object : Function0<Unit> {
                        override fun invoke() {

                        }

                    })
        }

    }

    @Inject
    lateinit var fileUtils: FileUtils

    override val mViewModel: HomeViewModel by viewModels()

    override fun layoutId() = R.layout.fragment_home

    override fun initView() {
        registerLocationUpdateReceiver()
        with(mDataBinding) {
            viewModel = mViewModel
            ivMenu.setOnClickListener(this@HomeFragment)
            llMap.setOnClickListener(this@HomeFragment)
            llCompassType.setOnClickListener(this@HomeFragment)
            llTheme.setOnClickListener(this@HomeFragment)
        }
        mCompassManager = CompassManager(requireContext(), this)
        mMagneticSensor = MagneticSensor(requireContext(), this)
        mSOTWFormatter = SOTWFormatter(requireContext())
    }

    private fun registerLocationUpdateReceiver() {
        val intentFilter = IntentFilter(BroadcastUtils.UPDATE_LOCATION)
        LocalBroadcastManager.getInstance(requireContext())
            .registerReceiver(this.locationUpdateReceiver, intentFilter);
    }


    override fun onPause() {
        super.onPause()
        mCompassManager?.stop()
        mMagneticSensor?.stop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        mCompassManager?.stop()
        mMagneticSensor?.stop()
    }

    override fun subscribeData() {
        with(mViewModel) {
            selectedCompassTheme.observe(this@HomeFragment) {
                Logger.d("Selected Compass: $it")
                applyThemeForCompass(it)
            }
            selectedBackgroundTheme.observe(this@HomeFragment) {
                Logger.d("Selected Background: $it")
                applyBackgroundTheme(it)
            }
        }
    }

    private fun applyBackgroundTheme(backgroundTheme: BackgroundTheme) {
        val file = fileUtils.imageFile(backgroundTheme)
        file?.let {
            if (it.exists() && it.length() > 0) {
                Glide.with(this).load(it).centerCrop().into(mDataBinding.ivBackground)
            }
        }
    }

    private fun applyThemeForCompass(compassTheme: CompassTheme) {
        if (mViewModel.isDefaultCompassTheme(compassTheme)) {
            Glide.with(this).load(R.drawable.default_compass_circle).into(mDataBinding.imgCompass)
            Glide.with(this).load(R.drawable.default_compass_plus).into(mDataBinding.imgPlus)
        } else {
            val circle = fileUtils.imageFile(compassTheme, ImageType.COMPASS_CIRCLE)
            val plus = fileUtils.imageFile(compassTheme, ImageType.COMPASS_PLUS)
            if (circle == null || plus == null) {
                return
            }
            with(mDataBinding) {
                Glide.with(this@HomeFragment).load(circle).into(imgCompass)
                Glide.with(this@HomeFragment).load(plus).into(imgPlus)
            }
        }

    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.ivMenu -> findNavController().navigate(HomeFragmentDirections.actionHomeToOtherFragment())
            R.id.llMap -> findNavController().navigate(HomeFragmentDirections.actionHomeToViewMapFragment())
            R.id.llCompassType -> showChooseCompassTypeDialog()
            R.id.llTheme -> findNavController().navigate(HomeFragmentDirections.actionHomeToSelectThemeFragment())
        }
    }

    private fun showChooseCompassTypeDialog() {
        val dialog = ChooseCompassTypeDialog()
        dialog.listener = this
        dialog.show(childFragmentManager, dialog.javaClass.canonicalName)
    }

    private fun loadCompassAndBackgroundTheme() {
        mViewModel.getSelectedBackgroundTheme()
        mViewModel.getSelectedCompassTheme()
    }

    override fun onResume() {
        super.onResume()
        loadCompassAndBackgroundTheme()
        lifecycleScope.launch(Dispatchers.IO) {
            selectedLanguage = multipleLanguageHelper.getSelectedLanguage()
            requireActivity().runOnUiThread {
                with(mDataBinding) {
                    tvMap.text = multipleLanguageHelper.getString(R.string.map, selectedLanguage)
                    tvCompassType.text =
                        multipleLanguageHelper.getString(R.string.compass_type, selectedLanguage)
                    tvTheme.text =
                        multipleLanguageHelper.getString(R.string.theme, selectedLanguage)
                    tvMagneticStrength.text =
                        multipleLanguageHelper.getString(
                            R.string.magnetic_strength,
                            selectedLanguage
                        )
                }
            }
        }
        mCompassManager?.start()
        mMagneticSensor?.start()
    }

    override fun onMoveCameraGoogleMap(f: Float) {

    }

    override fun onNewAzimuth(f: Float) {
        mDataBinding.tvRadian.text = mSOTWFormatter?.format(f)
        rotateImage(f)
    }

    private fun rotateImage(f: Float) {
        val rotateAnimation = RotateAnimation(-this.currentAzimuth, -f, 1, 0.5f, 1, 0.5f)
        this.currentAzimuth = f
        rotateAnimation.duration = 500
        rotateAnimation.repeatCount = 0
        rotateAnimation.fillAfter = true
        mDataBinding.imgCompass.startAnimation(rotateAnimation)
    }

    override fun invoke(p1: Double) {
        with(mDataBinding) {
            tvMagneticStrength.text =
                multipleLanguageHelper.getString(R.string.magnetic_strength, selectedLanguage)
                    .plus(" " + p1.toInt())
        }

    }

    override fun onFengshuiCompassSelected() {
        findNavController().navigate(
            HomeFragmentDirections.actionHomeToViewMapWithCompassFragment(1, location)
        )
    }

    override fun onMapCompassSelected() {
        findNavController().navigate(
            HomeFragmentDirections.actionHomeToViewMapWithCompassFragment(
                0,
                location
            )
        )
    }
}
