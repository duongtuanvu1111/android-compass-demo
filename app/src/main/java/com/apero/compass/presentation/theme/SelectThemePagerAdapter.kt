package com.apero.compass.presentation.theme

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.viewpager2.adapter.FragmentStateAdapter

class SelectThemePagerAdapter(fm: FragmentActivity) : FragmentStateAdapter(fm) {
    override fun getItemCount() = 2

    override fun createFragment(position: Int) = when (position) {
        0 -> SelectCompassThemeFragment()
        else -> SelectBackgroundThemeFragment()
    }
}