package com.apero.compass.presentation.theme

import android.os.Bundle
import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.apero.compass.R
import com.apero.compass.databinding.FragmentListCompassThemeBinding
import com.apero.compass.presentation.base.BaseFragment
import com.apero.compass.presentation.dialog.LoadingDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectCompassThemeFragment: BaseFragment<FragmentListCompassThemeBinding, SelectThemeViewModel>() {

    val loading: LoadingDialog by lazy {
        LoadingDialog()
    }

    override val mViewModel: SelectThemeViewModel by viewModels()

    override fun layoutId() = R.layout.fragment_list_compass_theme

    override fun initView() {
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.getListCompassTheme()
    }

    override fun subscribeData() {
        mViewModel.loading.observe(this) {
            when (it) {
                true -> loading.showDialog(childFragmentManager)
                else -> loading.hideDialog()
            }
        }

        mViewModel.listCompassTheme.observe(this) {
            val adapter = CompassThemeAdapter(it) {
                mViewModel.setCompassThemeSelected(it)
            }
            mDataBinding.rvTheme.adapter = adapter
        }

        mViewModel.selectCompassThemeResult.observe(this) {
            if (it) {
                findNavController().navigateUp()
            }
        }
    }
}