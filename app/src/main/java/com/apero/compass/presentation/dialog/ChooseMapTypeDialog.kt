package com.apero.compass.presentation.dialog

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.apero.compass.R
import com.apero.compass.databinding.DialogChooseCompassTypeBinding
import com.apero.compass.databinding.DialogChooseStyleMapBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ChooseMapTypeDialog : BottomSheetDialogFragment(), View.OnClickListener {

    lateinit var binding: DialogChooseStyleMapBinding
    var listener: ChooseMapTypeListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_choose_style_map,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnSatellite.setOnClickListener(this)
        binding.btnNormal.setOnClickListener(this)
    }

    override fun onClick(p0: View?) {
        dismiss()
        when (p0?.id) {
            R.id.btn_satellite -> listener?.onSatelliteSelected()
            R.id.btn_normal -> listener?.onNormalSelected()
        }

    }

}

interface ChooseMapTypeListener {
    fun onNormalSelected()
    fun onSatelliteSelected()
}
