package com.apero.compass.presentation.map.location

import android.app.Service
import android.content.Context
import android.content.Intent
import android.location.Location
import android.os.Build
import android.os.IBinder
import com.apero.compass.utils.BroadcastUtils
import com.apero.compass.utils.GpsUtils
import com.apero.compass.utils.NotifyUtils
import java.lang.String
import kotlin.Function1
import kotlin.TODO
import kotlin.Unit
import kotlin.jvm.internal.Intrinsics


class LocationService : Service() {
    companion object {
        const val LOCATION_NOTIFICATION_ID = 111
    }

    private var gpsUtils: GpsUtils? = null

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        startLocationService();
        gpsUtils = GpsUtils(this)
        gpsUtils?.stopRegisterLocationUpdate()
        gpsUtils?.registerUpdateLocationListener(
            false,
            object : Function1<Location, Unit> {
                override fun invoke(location: Location) {
                    BroadcastUtils.INSTANCE.notifyLocationChange(
                        this@LocationService,
                        String.valueOf(location.latitude),
                        String.valueOf(location.longitude)
                    )
                }
            },
            1
        )
    }

    override fun onDestroy() {
        super.onDestroy()
        gpsUtils?.stopRegisterLocationUpdate();

    }

    private fun startLocationService() {
        if (Build.VERSION.SDK_INT >= 26) {
            NotifyUtils.createNotificationChannel(
                NotifyUtils.INSTANCE,
                this,
                null,
                null,
                null,
                false,
                30,
                null
            )
            val notifyUtils: NotifyUtils = NotifyUtils.INSTANCE
            val applicationContext: Context = applicationContext
            Intrinsics.checkExpressionValueIsNotNull(applicationContext, "this.applicationContext")
            startForeground(
                111,
                NotifyUtils.createNotificationLocationBuilder(
                    notifyUtils,
                    applicationContext,
                    null,
                    false,
                    null,
                    null,
                    null,
                    58,
                    null
                ).build()
            )
        }
    }

}