package com.apero.compass.presentation.theme

import android.view.View
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.apero.compass.R
import com.apero.compass.databinding.FragmentSelectThemeBinding
import com.apero.compass.presentation.base.BaseFragment
import com.google.android.material.tabs.TabLayoutMediator
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class SelectThemeFragment : BaseFragment<FragmentSelectThemeBinding, SelectThemeViewModel>(),
    View.OnClickListener {

    override val mViewModel: SelectThemeViewModel by activityViewModels()

    override fun layoutId() = R.layout.fragment_select_theme

    override fun initView() {
        with(mDataBinding) {
            ivBack.setOnClickListener(this@SelectThemeFragment)
            vp.adapter = SelectThemePagerAdapter(requireActivity())
            TabLayoutMediator(tl, vp) { tab, position ->
                tab.text = when (position) {
                    0 -> getString(R.string.compass)
                    else -> getString(R.string.background)
                }
            }.attach()
        }
    }

    override fun subscribeData() {

    }

    override fun onClick(p0: View?) {
        findNavController().navigateUp()
    }

}