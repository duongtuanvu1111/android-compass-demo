package com.apero.compass.presentation.theme

import androidx.lifecycle.viewModelScope
import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.domain.exception.resolveError
import com.apero.compass.domain.usecase.*
import com.apero.compass.presentation.State
import com.apero.compass.presentation.base.BaseLiveData
import com.apero.compass.presentation.base.BaseViewModel
import com.apero.compass.utils.log.Logger
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SelectThemeViewModel @Inject constructor(
    private val getListCompassThemeUseCase: GetListCompassThemeUseCase,
    private val getListBackgroundThemeUseCase: GetListBackgroundThemeUseCase,
    private val setSelectedBackgroundThemeUseCase: SetSelectedBackgroundThemeUseCase,
    private val setSelectedCompassThemeUseCase: SetSelectedCompassThemeUseCase,
    private val downloadCompassThemeUseCase: DownloadCompassThemeUseCase,
    private val downloadBackgroundThemeUseCase: DownloadBackgroundThemeUseCase
) :
    BaseViewModel() {

    val listCompassTheme = BaseLiveData<List<CompassTheme>>()
    val listBackgroundTheme = BaseLiveData<List<BackgroundTheme>>()
    val selectCompassThemeResult = BaseLiveData<Boolean>()
    val selectBackgroundThemeResult = BaseLiveData<Boolean>()

    fun getListCompassTheme() = viewModelScope.launch(Dispatchers.IO) {
        getListCompassThemeUseCase.invoke(Unit).collect {
            when (it) {
                is State.LoadingState -> loading.postValue(true)
                is State.DataState -> {
                    loading.postValue(false)
                    listCompassTheme.postValue(it.data)
                    Logger.d("${it.data}")
                }
                is State.ErrorState -> {
                    Logger.d("Error: ${it.exception.resolveError()}")
                    loading.postValue(false)
                }
            }
        }
    }

    fun getListBackgroundTheme() = viewModelScope.launch (Dispatchers.IO) {
        getListBackgroundThemeUseCase.invoke(Unit).collect {
            when(it) {
                is State.LoadingState -> loading.postValue(true)
                is State.DataState -> {
                    loading.postValue(false)
                    listBackgroundTheme.postValue(it.data)
                }
                is State.ErrorState -> {
                    loading.postValue(false)
                }
            }
        }
    }

    fun setCompassThemeSelected(compassTheme: CompassTheme) = viewModelScope.launch (Dispatchers.IO) {

        downloadCompassThemeUseCase.invoke(compassTheme).collect {
            Logger.d("DownloadCompassThemeUseCase: ${it}")
            when(it) {
                is State.LoadingState -> loading.postValue(true)
                is State.DataState -> {
                    loading.postValue(false)
                    if (it.data) {
                        setSelectedCompassThemeUseCase.invoke(compassTheme).collect {
                            when(it) {
                                is State.LoadingState -> loading.postValue(true)
                                is State.DataState -> {
                                    loading.postValue(false)
                                    selectCompassThemeResult.postValue(it.data)
                                }
                            }
                        }
                    }
                }
                is State.ErrorState -> {
                    loading.postValue(false)
                    Logger.d(it.exception.resolveError().toString())
                }
            }
        }


    }

    fun setBackgroundThemeSelected(backgroundTheme: BackgroundTheme) = viewModelScope.launch (Dispatchers.IO) {
        downloadBackgroundThemeUseCase.invoke(backgroundTheme).collect {
            when(it) {
                is State.LoadingState -> loading.postValue(true)
                is State.DataState -> {
                    loading.postValue(false)
                    if (it.data) {
                        setSelectedBackgroundThemeUseCase.invoke(backgroundTheme).collect {
                            when(it) {
                                is State.LoadingState -> loading.postValue(true)
                                is State.DataState -> {
                                    loading.postValue(false)
                                    selectBackgroundThemeResult.postValue(it.data)
                                }
                            }
                        }
                    }
                }
            }
        }
    }

}