package com.apero.compass.presentation.language

import androidx.lifecycle.viewModelScope
import com.apero.compass.data.entity.Language
import com.apero.compass.data.entity.Languages
import com.apero.compass.domain.usecase.GetLanguageUseCase
import com.apero.compass.domain.usecase.SetLanguageUseCase
import com.apero.compass.presentation.State
import com.apero.compass.presentation.base.BaseLiveData
import com.apero.compass.presentation.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LanguageViewModel @Inject constructor(
    private val getLanguageUseCase: GetLanguageUseCase,
    private val setLanguageUseCase: SetLanguageUseCase
) :
    BaseViewModel() {

    val languages = BaseLiveData<Languages>()

    fun getLanguages() = viewModelScope.launch {
        getLanguageUseCase.invoke(Unit).collect {
            when (it) {
                is State.ErrorState -> {
                }
                is State.LoadingState -> {
                }
                is State.DataState -> languages.postValue(it.data)
            }
        }
    }

    fun setLanguage(language: Language)  = viewModelScope.launch {
        setLanguageUseCase.invoke(language).collect {
            when (it) {
                is State.ErrorState -> {
                }
                is State.LoadingState -> {
                }
                is State.DataState -> getLanguages()
            }
        }
    }
}