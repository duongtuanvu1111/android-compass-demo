package com.apero.compass.presentation.other

import android.view.View
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.apero.compass.BuildConfig
import com.apero.compass.R
import com.apero.compass.databinding.FragmentOtherBinding
import com.apero.compass.presentation.base.BaseFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class OtherFragment : BaseFragment<FragmentOtherBinding, OtherViewModel>(), View.OnClickListener {

    override val mViewModel: OtherViewModel by viewModels()

    override fun layoutId() = R.layout.fragment_other

    override fun initView() {
        with(mDataBinding) {
            ivBack.setOnClickListener(this@OtherFragment)
            llLanguage.setOnClickListener(this@OtherFragment)
            llTutorial.setOnClickListener(this@OtherFragment)
            llPolicy.setOnClickListener(this@OtherFragment)
            llRate.setOnClickListener(this@OtherFragment)
            llShare.setOnClickListener(this@OtherFragment)

            tvAppVersion.text = String.format("%s %s", getString(R.string.version), BuildConfig.VERSION_NAME)
        }
    }

    override fun subscribeData() {
    }

    override fun onClick(p0: View?) {
        when (p0?.id) {
            R.id.ivBack -> findNavController().navigateUp()
            R.id.llTutorial -> findNavController().navigate(OtherFragmentDirections.actionOtherFragmentToTutorialFragment())
            R.id.llLanguage -> findNavController().navigate(OtherFragmentDirections.actionOtherFragmentToLanguageFragment())
            R.id.llPolicy -> findNavController().navigate(OtherFragmentDirections.actionOtherFragmentToPolicyFragment())
            else -> {}
        }
    }
}