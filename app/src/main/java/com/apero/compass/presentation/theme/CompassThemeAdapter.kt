package com.apero.compass.presentation.theme

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.apero.compass.R
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.databinding.ItemCompassThemeBinding
import com.bumptech.glide.Glide

class CompassThemeAdapter(val themes: List<CompassTheme>, val onThemeSelected: (CompassTheme) -> Unit) :
    RecyclerView.Adapter<CompassThemeAdapter.CompassThemeViewHolder>() {

    class CompassThemeViewHolder(val binding: ItemCompassThemeBinding) :
        RecyclerView.ViewHolder(binding.root)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = CompassThemeViewHolder(
        DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.item_compass_theme,
            parent,
            false
        )
    )

    override fun onBindViewHolder(holder: CompassThemeViewHolder, position: Int) {
        holder.binding.theme = themes[position]
        holder.binding.root.setOnClickListener {
            onThemeSelected(themes[position])
        }
        holder.binding.executePendingBindings()
    }

    override fun getItemCount() = themes.size
}

@BindingAdapter("loadCompassThemeThumbnail")
fun loadThemeThumbnail(imageView: ImageView, compassTheme: CompassTheme) {
    Glide.with(imageView.context).load(compassTheme.thumbnail ?: "").into(imageView)
}