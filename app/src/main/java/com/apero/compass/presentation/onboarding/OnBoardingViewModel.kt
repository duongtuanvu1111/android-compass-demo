package com.apero.compass.presentation.onboarding

import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.apero.compass.data.local.Pref
import com.apero.compass.presentation.base.BaseViewModel
import com.apero.compass.utils.extension.SingleLiveEvent
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OnBoardingViewModel @Inject constructor(val pref: Pref) : BaseViewModel() {

    private val _actionToHome = SingleLiveEvent<Unit>()
    val actionToHome: LiveData<Unit> = _actionToHome

    private val _actionShowOnBoarding = SingleLiveEvent<Unit>()
    val actionShowOnBoarding: LiveData<Unit> = _actionShowOnBoarding

    init {
        viewModelScope.launch {
            delay(2000)
            if (pref.isHasShownOnBoarding()) {
                _actionToHome.value = Unit
            } else {
                _actionShowOnBoarding.value = Unit
            }
        }
    }

    fun saveStateOnBoarding() {
        pref.setOnBoarding(true)
    }
}