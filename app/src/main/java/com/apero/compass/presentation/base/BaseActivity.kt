package com.apero.compass.presentation.base

import androidx.appcompat.app.AppCompatActivity
import com.apero.compass.utils.extension.displayKeyboard

abstract class BaseActivity : AppCompatActivity() {

    override fun onPause() {
        super.onPause()
        displayKeyboard(false)
    }
}