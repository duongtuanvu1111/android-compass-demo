package com.apero.compass.presentation.map.withcompass

import android.os.Bundle
import android.view.View
import android.view.animation.RotateAnimation
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.apero.compass.R
import com.apero.compass.data.entity.Location
import com.apero.compass.data.local.Pref
import com.apero.compass.databinding.FragmentMapWithCompassBinding
import com.apero.compass.presentation.base.BaseFragment
import com.apero.compass.presentation.dialog.ChooseMapTypeDialog
import com.apero.compass.presentation.dialog.ChooseMapTypeListener
import com.apero.compass.utils.DialogUtils
import com.apero.compass.utils.format.SOTWFormatter
import com.apero.compass.utils.sensor.CompassManager
import com.apero.compass.utils.sensor.CompassWithMapManager
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject


@AndroidEntryPoint
class ViewMapWithCompassFragment :
    BaseFragment<FragmentMapWithCompassBinding, ViewMapWithCompassViewModel>(),
    View.OnClickListener,
    CompassManager.CompassListener, ChooseMapTypeListener {
    private var type: Int = 0
    private var location: Location? = null
    private val args: ViewMapWithCompassFragmentArgs by navArgs()
    private var currentAzimuth: Float = 0f
    private lateinit var compassSensor: CompassManager
    private lateinit var mCompassWithMapManager: CompassWithMapManager
    private var mSOTWFormatter: SOTWFormatter? = null
    override fun layoutId(): Int = R.layout.fragment_map_with_compass
    override val mViewModel: ViewMapWithCompassViewModel by viewModels()

    @Inject
    lateinit var pref: Pref

    override fun initView() {
        type = args.compassType
        location = args.location
        mCompassWithMapManager = CompassWithMapManager(
            requireContext(), childFragmentManager, DialogUtils.INSTANCE.checkPmsIsGranted(
                requireContext(),
                "android.permission.ACCESS_COARSE_LOCATION"
            ) && DialogUtils.INSTANCE.checkPmsIsGranted(
                requireContext(),
                "android.permission.ACCESS_FINE_LOCATION"
            )
        )
        mCompassWithMapManager.initMap(location, pref.getSelectedMapType())
        compassSensor = CompassManager(requireContext(), this)
        mSOTWFormatter = SOTWFormatter(requireContext())
        with(mDataBinding) {
            ivBack.setOnClickListener(this@ViewMapWithCompassFragment)
            ivExport.setOnClickListener(this@ViewMapWithCompassFragment)
            btnChangeMapType.setOnClickListener(this@ViewMapWithCompassFragment)
            titleMap.text =
                getString(if (type == 0) R.string.compass_type_map else R.string.fengshui_compass)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun subscribeData() {

    }

//    }

    override fun onClick(p0: View?) {
//        parentFragmentManager.popBackStack()
        when (p0?.id) {
            R.id.ivBack -> findNavController().navigateUp()
            R.id.btn_change_map_type -> {
                val dialog = ChooseMapTypeDialog()
                dialog.listener = this@ViewMapWithCompassFragment
                dialog.show(childFragmentManager, dialog.javaClass.canonicalName)
            }
        }
    }

    override fun onMoveCameraGoogleMap(f: Float) {
        mCompassWithMapManager.updateCamera(f)
    }

    override fun onNewAzimuth(f: Float) {
        mDataBinding.tvRadian.text = mSOTWFormatter?.format(f)
        val rotateAnimation = RotateAnimation(-this.currentAzimuth, -f, 1, 0.5f, 1, 0.5f)
        this.currentAzimuth = f
        rotateAnimation.duration = 500
        rotateAnimation.repeatCount = 0
        rotateAnimation.fillAfter = true
        mDataBinding.imgCompass.startAnimation(rotateAnimation)
    }

    override fun onResume() {
        super.onResume()
        bindThemeCompass()
        compassSensor.start()
    }

    private fun bindThemeCompass() {
        lifecycleScope.launch(Dispatchers.Main) {
            with(mDataBinding) {
//                imgCompass.setImageResource(if (type == 0) R.drawable.ic_frame else R.drawable.ic_group_60)
                Glide.with(requireActivity())
                    .load(if (type == 0) R.drawable.ic_frame else R.drawable.ic_group_compass)
                    .into(imgCompass)
                val size = if (type == 1) {
                    dp2px(60f)
                } else {
                    dp2px(150f)
                }
                imvPlusCenter.layoutParams.width = size
                imvPlusCenter.layoutParams.height = size
                imvPlusCenter.requestLayout()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        compassSensor.stop()
    }

    private fun dp2px(dpValue: Float): Int {
        val scale: Float = requireContext().resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }

    override fun onSatelliteSelected() {
        mCompassWithMapManager.changeTheme(2)
        if (type == 1) {
            return
        }
        Glide.with(this).load(R.drawable.ic_frame_compass_white).into(mDataBinding.imgCompass)
        pref.saveSelectedMapType(2)
    }

    override fun onNormalSelected() {
        mCompassWithMapManager.changeTheme(1)
        if (type == 1) {
            return
        }
        Glide.with(this).load(R.drawable.ic_frame).into(mDataBinding.imgCompass)
        pref.saveSelectedMapType(1)
    }


}