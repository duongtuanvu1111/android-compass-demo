package com.apero.compass.data.local

import android.content.Context
import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.data.entity.Languages
import com.google.gson.Gson
import javax.inject.Inject

class Pref @Inject constructor(
    private val context: Context,
    private val gson: Gson
) {

    companion object {
        const val PREF_NAME = "Apero_Compass_Pref"

        const val KEY_LANGUAGE = "language"
        const val KEY_ON_BOARDING = "onBoarding"
        const val KEY_COMPASS_THEME = "compass"
        const val KEY_BACKGROUND_THEME = "background"
        const val KEY_MAP_TYPE = "mapType" //normal = 1 ; satellite = 2
    }

    private fun getSharePreference() = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)

    fun setLanguages(languages: Languages) =
        getSharePreference().edit().putString(KEY_LANGUAGE, gson.toJson(languages)).commit()

    fun getLanguages(): Languages {
        val data = getSharePreference().getString(KEY_LANGUAGE, "")
        if (data.isNullOrEmpty()) {
            return Languages()
        } else {
            return gson.fromJson(data, Languages::class.java)
        }
    }

    fun setOnBoarding(isHasShown: Boolean) {
        getSharePreference().edit().putBoolean(KEY_ON_BOARDING, isHasShown).apply()
    }

    fun isHasShownOnBoarding(): Boolean = getSharePreference().getBoolean(KEY_ON_BOARDING, false)
    fun setSelectedCompassTheme(compassTheme: CompassTheme) =
        getSharePreference().edit().putString(KEY_COMPASS_THEME, gson.toJson(compassTheme)).commit()

    fun getSelectedCompassTheme(): CompassTheme? {
        val data = getSharePreference().getString(KEY_COMPASS_THEME, "")
        if (data.isNullOrEmpty()) {
            return null
        }
        return gson.fromJson(data, CompassTheme::class.java)
    }

    fun setSelectedBackgroundTheme(backgroundTheme: BackgroundTheme) =
        getSharePreference().edit().putString(
            KEY_BACKGROUND_THEME, gson.toJson(backgroundTheme)
        ).commit()

    fun getSelectedBackgroundTheme(): BackgroundTheme? {
        val data = getSharePreference().getString(KEY_BACKGROUND_THEME, "")
        if (data.isNullOrEmpty()) {
            return null
        }
        return gson.fromJson(data, BackgroundTheme::class.java)
    }

    fun saveSelectedMapType(type: Int) {
        if (type == 1 || type == 2) {
            getSharePreference().edit().putInt(KEY_MAP_TYPE, type).apply()
        }
    }

    fun getSelectedMapType() = getSharePreference().getInt(KEY_MAP_TYPE, 1)

}