package com.apero.compass.data.local

import com.apero.compass.R
import com.apero.compass.data.entity.Language
import com.apero.compass.domain.repository.LanguageRepository
import javax.inject.Inject

class MultipleLanguageHelper @Inject constructor(private val languageRepository: LanguageRepository) {

    suspend fun getSelectedLanguage() = languageRepository.getLanguages().languages.first {
        it.selected
    }

    fun getString(id: Int, language: Language): String {
        return when (id) {
            R.string.map -> when (language.code.uppercase()) {
                "EN" -> "Map"
                "VI" -> "Bản đồ"
                else -> "Map"
            }
            R.string.compass_type -> when (language.code.uppercase()) {
                "EN" -> "Compass Type"
                "VI" -> "Kiểu la bàn"
                else -> "Compass Type"
            }
            R.string.theme -> when (language.code.uppercase()) {
                "EN" -> "Theme"
                "VI" -> "Chủ đề"
                else -> "Theme"
            }
            R.string.magnetic_strength -> when (language.code.uppercase()) {
                "EN" -> "Magnetic strength"
                "VI" -> "Sức mạnh từ trường"
                else -> "Magnetic strength"
            }
            R.string.other -> when (language.code.uppercase()) {
                "EN" -> "Other"
                "VI" -> "Khác"
                else -> "Other"
            }
            R.string.language -> when (language.code.uppercase()) {
                "EN" -> "Language"
                "VI" -> "Ngôn ngữ"
                else -> "Language"
            }
            R.string.tutorial -> when (language.code.uppercase()) {
                "EN" -> "Compass Type"
                "VI" -> "Kiểu la bàn"
                else -> "Compass Type"
            }
            R.string.share -> when (language.code.uppercase()) {
                "EN" -> "Share"
                "VI" -> "Chia sẻ"
                else -> "Share"
            }
            R.string.rate -> when (language.code.uppercase()) {
                "EN" -> "Rate"
                "VI" -> "Đánh giá"
                else -> "rate"
            }
            R.string.tutorial_line_1 -> when (language.code.uppercase()) {
                "EN" -> "Swing your mobile several times in a figure 8 motion"
                "VI" -> "Xoay điện thoại di động của bạn nhiều lần theo chuyển động hình số 8"
                else -> "Swing your mobile several times in a figure 8 motion"
            }
            R.string.version -> when (language.code.uppercase()) {
                "EN" -> "Version"
                "VI" -> "Phiên bản"
                else -> "Version"
            }
            R.string.term_content -> when(language.code.uppercase()) {
                "EN" -> "Apero Studio built Trusted Compass as an Ad Supported app. This SERVICE is provided by Apero Studio at no cost and is intended for use as is.\n" +
                        "\n" +
                        "This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.\n" +
                        "\n" +
                        "If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.\n" +
                        "\n" +
                        "The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Document Scanner: Scan File unless otherwise defined in this Privacy Policy.\n" +
                        "\n" +
                        "Information Collection and Use\n" +
                        "\n" +
                        "For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information. The information that I request will be retained on your device and is not collected by me in any way.\n" +
                        ""
                else -> "Apero Studio built Trusted Compass as an Ad Supported app. This SERVICE is provided by Apero Studio at no cost and is intended for use as is.\n" +
                        "\n" +
                        "This page is used to inform visitors regarding my policies with the collection, use, and disclosure of Personal Information if anyone decided to use my Service.\n" +
                        "\n" +
                        "If you choose to use my Service, then you agree to the collection and use of information in relation to this policy. The Personal Information that I collect is used for providing and improving the Service. I will not use or share your information with anyone except as described in this Privacy Policy.\n" +
                        "\n" +
                        "The terms used in this Privacy Policy have the same meanings as in our Terms and Conditions, which is accessible at Document Scanner: Scan File unless otherwise defined in this Privacy Policy.\n" +
                        "\n" +
                        "Information Collection and Use\n" +
                        "\n" +
                        "For a better experience, while using our Service, I may require you to provide us with certain personally identifiable information. The information that I request will be retained on your device and is not collected by me in any way.\n" +
                        ""
            }
            else -> ""
        }
    }
}