package com.apero.compass.data.repository

import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.data.local.Pref
import com.apero.compass.data.remote.Api
import com.apero.compass.domain.repository.ThemeRepository
import javax.inject.Inject

class ThemeRepositoryImpl @Inject constructor(private val api: Api, private val pref: Pref) :
    ThemeRepository {
    override suspend fun getListCompassTheme() = api.getCompassTheme()

    override suspend fun getListBackgroundTheme() = api.getBackgroundTheme()

    override suspend fun getSelectedCompassTheme() = pref.getSelectedCompassTheme()

    override suspend fun getSelectedBackgroundTheme() = pref.getSelectedBackgroundTheme()

    override suspend fun setSelectedCompassTheme(compassTheme: CompassTheme) =
        pref.setSelectedCompassTheme(compassTheme)

    override suspend fun setSelectedBackgroundTheme(backgroundTheme: BackgroundTheme) =
        pref.setSelectedBackgroundTheme(backgroundTheme)
}