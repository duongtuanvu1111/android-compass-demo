package com.apero.compass.data.remote

import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.data.entity.User
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Streaming
import retrofit2.http.Url

interface Api {
    @GET("compass.json")
    suspend fun getCompassTheme(): List<CompassTheme>

    @GET("background.json")
    suspend fun getBackgroundTheme(): List<BackgroundTheme>

    @Streaming
    @GET
    suspend fun downloadFile(@Url url: String): ResponseBody
}