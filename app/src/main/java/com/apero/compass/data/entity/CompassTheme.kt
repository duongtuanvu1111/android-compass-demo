package com.apero.compass.data.entity

import com.google.gson.annotations.SerializedName

data class CompassTheme(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("thumbnail") var thumbnail: String? = null,
    @SerializedName("circle") var circle: String? = null,
    @SerializedName("plus") var plus: String? = null
) {
    var selected = false

}