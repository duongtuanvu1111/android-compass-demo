package com.apero.compass.data.local

import android.content.Context
import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.presentation.App
import com.apero.compass.utils.log.Logger
import okhttp3.ResponseBody
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import javax.inject.Inject


class FileUtils @Inject constructor(val context: Context) {

    fun saveBackground(theme: BackgroundTheme, body: ResponseBody) : String {
        val fileName = "background_${theme.id}.png"
        return saveFile(fileName, body)
    }

    fun saveCompassThumb(theme: CompassTheme, body: ResponseBody, type: ImageType): String {
        val fileName = when(type) {
            ImageType.COMPASS_THUMB -> "compass_thumb_${theme.id}.png"
            ImageType.COMPASS_CIRCLE -> "compass_circle_${theme.id}.png"
            ImageType.COMPASS_PLUS -> "compass_plus_${theme.id}.png"
            ImageType.BACKGROUND -> "background_${theme.id}.png"
        }
        return saveFile(fileName, body)
    }

    fun imageFile(theme: BackgroundTheme): File? {
        val fileName = "background_${theme.id}.png"
        val file = File(getRootTheme().absolutePath + File.separator + fileName)
        if (file.exists() && file.length() > 0) {
            return file
        }
        return null
    }

    fun imageFile(theme: CompassTheme, type: ImageType): File? {
        val fileName = when(type) {
            ImageType.COMPASS_THUMB -> "compass_thumb_${theme.id}.png"
            ImageType.COMPASS_CIRCLE -> "compass_circle_${theme.id}.png"
            ImageType.COMPASS_PLUS -> "compass_plus_${theme.id}.png"
            ImageType.BACKGROUND -> "background_${theme.id}.png"
        }
        val file = File(getRootTheme().absolutePath + File.separator + fileName)
        if (file.exists() && file.length() > 0) {
            return file
        }
        return null
    }

    fun isValidFile(filePath: String): Boolean {
        val f = File(filePath)
        Logger.d("Checking file: $filePath ; exists: ${f.exists()} ; size: ${f.length()}")
        return f.exists() && f.length() > 0
    }

    private fun getRootTheme(): File {
        val folder = File(
            App.app.filesDir,
            APP_FOLDER
        )
        if (!folder.exists()) folder.mkdirs()
        return folder
    }

    private fun saveFile(fileName: String, body: ResponseBody): String {
        var input: InputStream? = null
        try {
            input = body.byteStream()
            val path =
                getRootTheme().absolutePath + File.separator + fileName

            val testFile = File(path)
            if(testFile.exists() && testFile.length() > 0) {
                return path
            }
            //val file = File(getCacheDir(), "cacheFileAppeal.srl")
            val fos = FileOutputStream(path)
            fos.use { output ->
                val buffer = ByteArray(4 * 1024) // or other buffer size
                var read: Int
                while (input.read(buffer).also { read = it } != -1) {
                    output.write(buffer, 0, read)
                }
                output.flush()
            }
            return path
        } catch (e: Exception) {
            Logger.d("saveFile ${e.toString()}")
        } finally {
            input?.close()
        }
        return ""
    }

}

enum class ImageType {
    COMPASS_THUMB, COMPASS_CIRCLE, COMPASS_PLUS, BACKGROUND
}

const val APP_FOLDER = "Apero_Compass"