package com.apero.compass.data.entity

import com.google.gson.annotations.SerializedName

data class BackgroundTheme(
    @SerializedName("id") var id: Int? = null,
    @SerializedName("thumbnail") var thumbnail: String? = null,
    @SerializedName("file") var file: String? = null
) {
    var selected = false
}