package com.apero.compass.data.entity

import java.io.Serializable

data class Location(var lat: String, var lng: String) : Serializable