package com.apero.compass.data.entity

data class User (val name: String)