package com.apero.compass.data.entity

data class Language(val code: String, val title: String, var selected: Boolean)

class Languages(val languages: List<Language> = Languages.prepareLanguageList()) {

    companion object {
        fun prepareLanguageList(): List<Language> {
            return mutableListOf(
                Language("en", "English", true),
                Language("vi", "Vietnamese", false),
                Language("in", "Indonesia", false),
                Language("cn", "China", false)
            )
        }
    }

}