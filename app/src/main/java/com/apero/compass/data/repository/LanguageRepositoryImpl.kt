package com.apero.compass.data.repository

import com.apero.compass.data.entity.Languages
import com.apero.compass.data.local.Pref
import com.apero.compass.domain.repository.LanguageRepository
import javax.inject.Inject

class LanguageRepositoryImpl @Inject constructor(private val pref: Pref) : LanguageRepository {
    override fun getLanguages() = pref.getLanguages()

    override fun setLanguages(languages: Languages) = pref.setLanguages(languages)
}