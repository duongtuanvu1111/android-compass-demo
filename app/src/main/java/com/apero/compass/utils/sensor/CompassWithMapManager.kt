package com.apero.compass.utils.sensor

import android.annotation.SuppressLint
import android.content.Context
import androidx.fragment.app.FragmentManager
import com.apero.compass.R
import com.apero.compass.data.entity.Location
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker

class CompassWithMapManager(
    private val context: Context,
    private val fragmentManager: FragmentManager,
    private val isGranted: Boolean
) : OnMapReadyCallback, GoogleMap.OnCameraMoveListener, GoogleMap.OnCameraIdleListener {
    private val mCurrentLocationMarker: Marker? = null
    private var mMap: GoogleMap? = null
    private var latestKnowLocation: Location? = null
    private var initMapType = 1
    init {
    }

    fun initMap( latestKnowLocation: Location?, initMapType: Int) {
        this.latestKnowLocation = latestKnowLocation
        this.initMapType = initMapType
        var supportMapFragment: SupportMapFragment?
        if (mMap == null) {
            supportMapFragment =
                (fragmentManager.findFragmentById(R.id.map) as SupportMapFragment)
            supportMapFragment.getMapAsync(this)
        }
        if (mMap != null) {
            setupMap()
        }
    }

    @SuppressLint("MissingPermission")
    private fun setupMap() {
        if (mMap == null) {
            return
        }
        with(mMap!!) {
            isBuildingsEnabled = true
            if (isGranted) {
                isMyLocationEnabled = true
            }
            val uiSettings: UiSettings = uiSettings
            uiSettings.setAllGesturesEnabled(false)
            uiSettings.isCompassEnabled = false
            uiSettings.isMyLocationButtonEnabled = false
            uiSettings.isRotateGesturesEnabled = false
            uiSettings.isTiltGesturesEnabled = false
            uiSettings.isMapToolbarEnabled = false
            uiSettings.isScrollGesturesEnabled = false
            uiSettings.isZoomControlsEnabled = false
            uiSettings.isZoomGesturesEnabled = true
        }
        mMap?.setOnCameraMoveListener(this)
        mMap?.setOnCameraIdleListener(this)
        changeTheme(1)
    }

    fun changeTheme(i: Int) {
        if (mMap == null) {
            return
        }
        if (mMap?.mapType == i) {
            return
        }
        mMap?.mapType = i
    }

    fun updateCamera(f: Float) {
        if (mMap == null) {
            return
        }
        with(mMap!!) {
            val cameraUpdate = CameraUpdateFactory.newCameraPosition(
                CameraPosition.builder(
                    cameraPosition
                ).bearing(f).build()
            )
            moveCamera(cameraUpdate)
        }

    }

    fun moveToLocation(location: Location) {
        mMap?.let {
            //it.animateCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.lat.toDouble(), location.lng.toDouble()), DEFAULT_ZOOM_LEVEL))
            it.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(location.lat.toDouble(), location.lng.toDouble()), DEFAULT_ZOOM_LEVEL))
        }
    }

    override fun onMapReady(p0: GoogleMap?) {
        var d: Double
        mMap = p0
        setupMap()
        latestKnowLocation?.let {
            moveToLocation(it)
        }
        changeTheme(initMapType)
    }

    override fun onCameraMove() {

    }

    override fun onCameraIdle() {

    }


}

const val DEFAULT_ZOOM_LEVEL = 12f