package com.apero.compass.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Context.LOCATION_SERVICE
import android.location.Location
import android.location.LocationManager
import android.location.LocationManager.GPS_PROVIDER
import android.location.LocationManager.NETWORK_PROVIDER
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnSuccessListener


class GpsUtils(private val context: Context) {
    private var builder: LocationSettingsRequest.Builder? = null
    private var fusedLocationClient: FusedLocationProviderClient
    private var isCurrentRequest: Boolean = false
    private lateinit var locationCallback: LocationCallback
    private var locationManager: LocationManager
    private var locationRequest: LocationRequest = LocationRequest.create()
    private lateinit var onUpdateLocationListener: Function1<Location, Unit>
    private var settingClient: SettingsClient = LocationServices.getSettingsClient(context)

    init {
        locationRequest.priority = 100
        locationRequest.interval = 8000
        locationRequest.fastestInterval = 4000
        locationManager = context.getSystemService(LOCATION_SERVICE) as LocationManager
        builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context)
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult) {
                super.onLocationResult(p0)
                val lastLocation = p0.lastLocation
                onUpdateLocationListener.invoke(lastLocation)
                if (isCurrentRequest) {
                    fusedLocationClient.removeLocationUpdates(locationCallback)
                }
            }

            override fun onLocationAvailability(p0: LocationAvailability) {
                super.onLocationAvailability(p0)
            }
        }

    }

    fun getContext(): Context {
        return context
    }

    private fun isEnableGPS(): Boolean {
        return isGPSProviderEnable() || isNetworkProviderEnable()
    }

    private fun isNetworkProviderEnable(): Boolean {
        return this.locationManager.isProviderEnabled(NETWORK_PROVIDER);

    }

    private fun isGPSProviderEnable(): Boolean {
        return locationManager.isProviderEnabled(GPS_PROVIDER)
    }

    fun registerUpdateLocationListener(z: Boolean, function: Function1<Location, Unit>?, i: Int) {
        var flag = z
        if (i and 1 != 0) {
            flag = false
        }
        var tempFunction = function
        if (i and 2 != 0) {
            tempFunction = null
        }
        registerUpdateLocationListener(flag, tempFunction)
    }

    @SuppressLint("MissingPermission")
    fun registerUpdateLocationListener(z: Boolean, function1: Function1<Location, Unit>?) {
        onUpdateLocationListener = function1!!
        fusedLocationClient.removeLocationUpdates(locationCallback)
        isCurrentRequest = z
        if (z) {
            val fusedLocationProviderClient = fusedLocationClient
            fusedLocationProviderClient.lastLocation.addOnSuccessListener { p0 ->
                p0?.let {
                    onUpdateLocationListener.invoke(
                        it
                    )
                }
            }.addOnFailureListener { p0 -> p0.printStackTrace() }
        } else {
            fusedLocationClient.requestLocationUpdates(
                locationRequest,
                locationCallback,
                Looper.getMainLooper()
            )
        }
        if (!isEnableGPS()) {
            settingClient.checkLocationSettings(builder!!.build())
                .addOnSuccessListener(object : OnSuccessListener<LocationSettingsResponse> {
                    override fun onSuccess(p0: LocationSettingsResponse?) {

                    }
                })
                .addOnFailureListener { exc ->
                    if (exc is ResolvableApiException && getContext() is AppCompatActivity) {
                        try {
                            exc.startResolutionForResult(getContext() as Activity, 111)
                        } catch (unused: Exception) {
                        }
                    }
                }
        }
    }

    fun stopRegisterLocationUpdate() {
        fusedLocationClient.removeLocationUpdates(locationCallback)
    }
}