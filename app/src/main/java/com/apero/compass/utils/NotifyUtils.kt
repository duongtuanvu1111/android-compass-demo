package com.apero.compass.utils

import android.R
import android.app.Notification.VISIBILITY_PUBLIC
import android.app.NotificationChannel
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.content.Context
import android.graphics.BitmapFactory
import android.media.AudioAttributes
import android.media.AudioAttributes.*
import android.media.RingtoneManager
import android.net.Uri
import android.os.Build
import android.provider.Settings.System.DEFAULT_NOTIFICATION_URI
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import kotlin.jvm.internal.Intrinsics


class NotifyUtils {
    companion object {
        val CHANNEL_ID: String? = null
        private val CHANNEL_NAME: String? = null
        val INSTANCE = NotifyUtils()
        const val NOTIFICATION_TRIP_ID = 0
        private val defaultVibratePattern = longArrayOf(0, 250, 250, 250)
        fun createNotificationChannel(
            notifyUtils: NotifyUtils,
            context: Context?,
            str: String?,
            str2: String?,
            str3: String?,
            z: Boolean,
            i: Int,
            obj: Any?
        ): NotificationChannel? {
            var str = str
            var str2 = str2
            var str3 = str3
            if (i and 2 != 0) {
                str = CHANNEL_ID
            }
            if (i and 4 != 0) {
                str2 = CHANNEL_NAME
            }
            if (i and 8 != 0) {
                str3 = "Compass Channel notification"
            }
            return notifyUtils.createNotificationChannel(
                context!!,
                str,
                str2,
                str3,
                if (i and 16 != 0) true else z
            )
        }

        fun createNotificationLocationBuilder(
            notifyUtils: NotifyUtils,
            context: Context,
            uri: Uri?,
            z: Boolean,
            pendingIntent: PendingIntent?,
            str: String?,
            str2: String?,
            i: Int,
            obj: Any?
        ): NotificationCompat.Builder {
            return notifyUtils.createNotificationLocationBuilder(
                context,
                if (i and 2 != 0) null else uri,
                if (i and 4 != 0) true else z,
                if (i and 8 != 0) null else pendingIntent,
                if (i and 16 != 0) CHANNEL_NAME else str,
                if (i and 32 != 0) "This app got your location in the background." else str2
            )
        }

    }


    fun createNotificationChannel(
        context: Context,
        str: String?,
        str2: String?,
        str3: String?,
        z: Boolean
    ): NotificationChannel? {
        if (Build.VERSION.SDK_INT < 26) {
            return null
        }
        val build: AudioAttributes =
            Builder().setUsage(USAGE_NOTIFICATION).setContentType(CONTENT_TYPE_SONIFICATION).build()
        val defaultUri: Uri = RingtoneManager.getDefaultUri(2)
        val notificationChannel = NotificationChannel(str, str2, IMPORTANCE_HIGH)
        notificationChannel.description = str3
        notificationChannel.lockscreenVisibility = 1
        if (z) {
            notificationChannel.setSound(defaultUri, build)
        } else {
            notificationChannel.setSound(null, null)
        }
        notificationChannel.vibrationPattern = defaultVibratePattern
        notificationChannel.enableVibration(true)
        NotificationManagerCompat.from(context).createNotificationChannel(notificationChannel)
        return notificationChannel
    }

    fun createNotificationLocationBuilder(
        context: Context,
        uri: Uri?,
        z: Boolean,
        pendingIntent: PendingIntent?,
        str: String?,
        str2: String?
    ): NotificationCompat.Builder {
        var tempUri = uri
        Intrinsics.checkParameterIsNotNull(context, "context")
        Intrinsics.checkParameterIsNotNull(str, "title")
        val autoCancel: NotificationCompat.Builder = NotificationCompat.Builder(
            context,
            CHANNEL_ID!!
        ).setContentTitle(str).setContentText(str2).setVibrate(defaultVibratePattern).setPriority(2)
            .setContentIntent(pendingIntent).setSmallIcon(R.drawable.ic_menu_compass)
            .setLargeIcon(
                BitmapFactory.decodeResource(
                    context.resources,
                    R.mipmap.sym_def_app_icon
                )
            )
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC).setAutoCancel(true)
        if (tempUri == null) {
            tempUri = DEFAULT_NOTIFICATION_URI
        }
        val sound = autoCancel.setSound(tempUri)
        Intrinsics.checkExpressionValueIsNotNull(sound, "notification")
        return sound
    }

}