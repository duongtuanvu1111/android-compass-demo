package com.apero.compass.utils.sensor

import android.content.Context
import android.content.Context.SENSOR_SERVICE
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager


class CompassManager(context: Context, compassListener: CompassListener) : SensorEventListener {
    private val i = FloatArray(9)
    private val r = FloatArray(9)
    private var azimuth = 0f
    private var azimuthFix = 0f
    private val mContext: Context = context
    private val compassListener: CompassListener = compassListener
    private val gSensor: Sensor
    private val mGeomagnetic = FloatArray(3)
    private val mGravity = FloatArray(3)
    private val mSensor: Sensor
    private val mapSensor: Sensor
    private val sensorManager: SensorManager =
        (context.getSystemService(SENSOR_SERVICE) as SensorManager)


    override fun onSensorChanged(p0: SensorEvent?) {
        synchronized(this) {
            with(p0!!) {
                val sensor: Sensor = this.sensor
                if (sensor.type == Sensor.TYPE_ACCELEROMETER) {
                    val f = 1.toFloat() - 0.97f
                    mGravity[0] =
                        mGravity[0] * 0.97f + this.values[0] * f
                    mGravity[1] =
                        mGravity[1] * 0.97f + this.values[1] * f
                    mGravity[2] =
                        mGravity[2] * 0.97f + f * this.values[2]
                }
                if (sensor.type == Sensor.TYPE_MAGNETIC_FIELD) {
                    val f2 = 1.toFloat() - 0.97f
                    mGeomagnetic[0] =
                        mGeomagnetic[0] * 0.97f + this.values[0] * f2
                    mGeomagnetic[1] =
                        mGeomagnetic[1] * 0.97f + this.values[1] * f2
                    mGeomagnetic[2] =
                        mGeomagnetic[2] * 0.97f + f2 * this.values[2]
                }
                if (SensorManager.getRotationMatrix(r, i, mGravity, mGeomagnetic)) {
                    val fArr = FloatArray(3)
                    SensorManager.getOrientation(r, fArr)
                    val degrees =
                        Math.toDegrees(fArr[0].toDouble()).toFloat()
                    azimuth = degrees
                    val f3 = 360.toFloat()
                    val f4 = (degrees + azimuthFix + f3) % f3
                    azimuth = f4
                    compassListener.onNewAzimuth(f4)
                    if (sensor.type == Sensor.TYPE_ROTATION_VECTOR) {
                        compassListener.onMoveCameraGoogleMap(azimuth)
                    }
                }
            }
        }

    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    init {
        this.gSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        this.mSensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
        this.mapSensor = this.sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR)
    }

    fun start() {
        sensorManager.registerListener(this, gSensor, SensorManager.SENSOR_DELAY_GAME)

        sensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_GAME)

        sensorManager.registerListener(this, mapSensor, SensorManager.SENSOR_DELAY_UI)
    }

    fun stop() {
        sensorManager.unregisterListener(this, gSensor)
        sensorManager.unregisterListener(this, mSensor)
        sensorManager.unregisterListener(this, mapSensor)
    }

    fun setAzimuthFix(f: Float) {
        azimuthFix = f
    }

    fun resetAzimuthFix() {
        setAzimuthFix(0.0f)
    }


    interface CompassListener {
        fun onMoveCameraGoogleMap(f: Float)
        fun onNewAzimuth(f: Float)
    }


}