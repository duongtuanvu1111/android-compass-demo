package com.apero.compass.utils

import android.content.Context
import android.location.Address
import android.location.Geocoder
import android.location.Location
import com.apero.compass.R
import java.util.*
import kotlin.math.abs


class LocationUtils {
    companion object {
        val INSTANCE = LocationUtils()
    }

    fun getAddressWithLatLng(
        context: Context,
        d: Double,
        d2: Double,
        function: () -> Unit,
        i: Int
    ): String {
        return getAddressWithLatLng(context, d, d2, function)
    }

    fun getAddressWithLatLng(
        context: Context,
        d: Double,
        d2: Double,
        function0: Function0<Unit>
    ): String {
        return try {
            val addressLine = Geocoder(context, Locale.getDefault()).getFromLocation(
                d,
                d2,
                1
            )[0].getAddressLine(0)
            addressLine ?: ""
        } catch (unused: Exception) {
            function0.invoke()
            val string = context.getString(R.string.unable_determine_location)
            string
        }
    }

    fun getCountryAndCityWithLatLng(
        context: Context?,
        d: Double,
        d2: Double,
        function0: Function0<Unit>
    ): String {
        return try {
            val fromLocation: List<Address> =
                Geocoder(context, Locale.getDefault()).getFromLocation(d, d2, 1)
            fromLocation[0].getAddressLine(0)
            val sb = StringBuilder()
            val address: Address = fromLocation[0]
            sb.append(address.locality)
            sb.append(", ")
            val address2: Address = fromLocation[0]
            sb.append(address2.countryName)
            sb.toString()
        } catch (unused: java.lang.Exception) {
            function0.invoke()
            ""
        }
    }

    fun convertLatLngToString(lat: Double, lng: Double): String {
        val sb = StringBuilder()
        sb.append(convertLatToString(lat))
        sb.append("    ")
        sb.append(convertLngToString(lng))
        return sb.toString()
    }

    fun convertLatToString(d: Double): String {
        val sb = java.lang.StringBuilder()
        val convert = Location.convert(abs(d), 2)
        val split = convert.split(":")
        sb.append(split[0])
        sb.append("°")
        sb.append(split[1])
        sb.append("'")
        sb.append(split[2])
        sb.append("\"")
        if (d < 0) {
            sb.append(" S")
        } else {
            sb.append(" N")
        }
        return sb.toString()
    }

    fun convertLngToString(d: Double): String {
        val sb = java.lang.StringBuilder()
        val convert = Location.convert(abs(d), 2)
        val split = convert.split(":")
        sb.append(split[0])
        sb.append("°")
        sb.append(split[1])
        sb.append("'")
        sb.append(split[2])
        sb.append("\"")
        if (d < 0) {
            sb.append(" W")
        } else {
            sb.append(" E")
        }
        return sb.toString()
    }


}