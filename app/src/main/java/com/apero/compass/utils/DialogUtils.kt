package com.apero.compass.utils

import android.content.Context
import android.content.pm.PackageManager.PERMISSION_GRANTED
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentManager


class DialogUtils {
    companion object {
        val INSTANCE = DialogUtils()
        const val REQUEST_PERMISSION_COMPASS_MAP = 0
        const val REQUEST_PERMISSION_LOCATION = 0
        const val REQUEST_PERMISSION_MAP = 0
        private val arrPms = arrayOf(
            "android.permission.ACCESS_FINE_LOCATION",
            "android.permission.ACCESS_COARSE_LOCATION"
        )

    }

    fun checkOnShowRequestLocationPermission(
        appCompatActivity: AppCompatActivity,
        i: Int,
        function0: Function0<Unit>
    ) {
        if (!checkPmsIsGranted(
                appCompatActivity,
                "android.permission.ACCESS_COARSE_LOCATION"
            ) || !checkPmsIsGranted(
                appCompatActivity,
                "android.permission.ACCESS_FINE_LOCATION"
            )
        ) {
            askPms(appCompatActivity, i)
        } else {
            function0.invoke()
        }
    }

    private fun askPms(appCompatActivity: AppCompatActivity, i: Int) {
        ActivityCompat.requestPermissions(appCompatActivity, arrPms, i);

    }

    fun checkPmsIsGranted(context: Context, str: String): Boolean {
        return ContextCompat.checkSelfPermission(context, str) == PERMISSION_GRANTED
    }

    fun isLocationPermissionGranted(context: Context): Boolean {
        return checkPmsIsGranted(
            context,
            "android.permission.ACCESS_COARSE_LOCATION"
        ) && checkPmsIsGranted(
            context, "android.permission.ACCESS_FINE_LOCATION"
        )
    }

}