package com.apero.compass.utils.format;

import android.content.Context;

import com.apero.compass.R;

public class SOTWFormatter {
    private static String[] names = null;
    private static final int[] sides = {0, 45, 90, 135, 180, 225, 270, 315, 360};

    public SOTWFormatter(Context context) {
        initLocalizedNames(context);
    }

    public String format(float f) {
        int i = (int) f;
        int findClosestIndex = findClosestIndex(i);
        return i + "° " + names[findClosestIndex];
    }

    private void initLocalizedNames(Context context) {
        if (names == null) {
            names = new String[]{context.getString(R.string.sotw_north), context.getString(R.string.sotw_northeast), context.getString(R.string.sotw_east), context.getString(R.string.sotw_southeast), context.getString(R.string.sotw_south), context.getString(R.string.sotw_southwest), context.getString(R.string.sotw_west), context.getString(R.string.sotw_northwest), context.getString(R.string.sotw_north)};
        }
    }

    private static int findClosestIndex(int i) {
        int length = sides.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            i3 = (i2 + length) / 2;
            int[] iArr = sides;
            if (i < iArr[i3]) {
                if (i3 > 0) {
                    int i4 = i3 - 1;
                    if (i > iArr[i4]) {
                        return getClosest(i4, i3, i);
                    }
                }
                length = i3;
            } else {
                if (i3 < iArr.length - 1) {
                    int i5 = i3 + 1;
                    if (i < iArr[i5]) {
                        return getClosest(i3, i5, i);
                    }
                }
                i2 = i3 + 1;
            }
        }
        return i3;
    }

    private static int getClosest(int i, int i2, int i3) {
        int[] iArr = sides;
        return i3 - iArr[i] >= iArr[i2] - i3 ? i2 : i;
    }
}