package com.apero.compass.utils

import android.content.Context
import android.content.Intent

import androidx.localbroadcastmanager.content.LocalBroadcastManager

import kotlin.jvm.internal.Intrinsics


class BroadcastUtils {
    companion object {
        val INSTANCE = BroadcastUtils()
        const val UPDATE_LOCATION = "UPDATE_LOCATION"
    }


    fun notifyLocationChange(context: Context, str: String?, str2: String?) {
        val intent = Intent(UPDATE_LOCATION)
        intent.putExtra("latitude", str)
        intent.putExtra("longitude", str2)
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent)
    }

}