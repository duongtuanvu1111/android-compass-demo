package com.apero.compass.utils.log

import com.apero.compass.BuildConfig


object Logger {

    private val LOG = BuildConfig.LOGGABLE
    private val TAG = "Compass"

    /**
     * Log debug

     * @param message message to log
     */
    fun d(message: String) {
        if (LOG) {
            com.orhanobut.logger.Logger.d(message)
        }
    }

    fun json(jsonObject: String) {
        if (LOG) {
            com.orhanobut.logger.Logger.json(jsonObject)
        }
    }

    fun add(a: Int, b: Int) = a + b
}