package com.apero.compass.utils.sensor

import android.content.Context
import android.hardware.Sensor
import android.hardware.Sensor.TYPE_MAGNETIC_FIELD
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.hardware.SensorManager.SENSOR_DELAY_NORMAL
import kotlin.math.sqrt


class MagneticSensor(context: Context, function1: Function1<Double, Unit>) : SensorEventListener {
    private val mContext: Context = context
    private val magneticSensor: Sensor
    private val onListener: Function1<Double, Unit> = function1
    private val sensorManager: SensorManager =
        mContext.getSystemService(Context.SENSOR_SERVICE) as SensorManager

    init {
        magneticSensor = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD)
    }

    override fun onSensorChanged(p0: SensorEvent?) {
        synchronized(this) {
            with(p0!!) {
                val sensor: Sensor = this.sensor
                if (sensor.type == TYPE_MAGNETIC_FIELD) {
                    onListener.invoke(sqrt((this.values[0] * this.values[0]).toDouble() + (values[1] * values[1]).toDouble() + (values[2] * values[2]).toDouble()))
                }
            }
        }
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    fun getOnListener(): Function1<Double, Unit> {
        return onListener
    }

    fun start() {
        sensorManager.registerListener(this, magneticSensor, SENSOR_DELAY_NORMAL)
    }

    fun stop() {
        sensorManager.unregisterListener(this)
    }


}