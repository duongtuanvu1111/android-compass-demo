package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.local.Pref
import com.apero.compass.data.remote.Api
import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.flow.zip
import java.lang.Exception
import javax.inject.Inject

class GetListBackgroundThemeUseCase @Inject constructor(
    private val api: Api,
    private val pref: Pref
) : UseCase<List<BackgroundTheme>, Unit>() {
    override fun invoke(param: Unit): Flow<State<List<BackgroundTheme>>> {
        val getListBackgroundThemeFlow = flow {
            try {
                emit(State.DataState(api.getBackgroundTheme()))
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }

        val getSelectedBackgroundThemeFlow = flow {
            emit(State.DataState(pref.getSelectedBackgroundTheme()))
        }

        return getListBackgroundThemeFlow.zip(getSelectedBackgroundThemeFlow) { listTheme, selectedTheme ->
            if (listTheme is State.DataState) {
                State.DataState(listTheme.data.map {
                    it.selected = it.id == selectedTheme.data?.id
                })
            }
            listTheme
        }.onStart {
            emit(State.LoadingState)
        }
    }
}