package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.Languages
import com.apero.compass.domain.repository.LanguageRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import java.lang.Exception
import javax.inject.Inject

class GetLanguageUseCase @Inject constructor(private val languageRepository: LanguageRepository) :
    UseCase<Languages, Unit>() {
    override fun invoke(param: Unit): Flow<State<Languages>> {
        return flow {
            emit(State.LoadingState)
            try {
                val languages = languageRepository.getLanguages()
                if (languages.languages.isNotEmpty()) {
                    emit(State.DataState(languageRepository.getLanguages()))
                } else {
                    emit(State.ErrorState(Throwable("No language detected")))
                }
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }.flowOn(Dispatchers.IO)
    }
}