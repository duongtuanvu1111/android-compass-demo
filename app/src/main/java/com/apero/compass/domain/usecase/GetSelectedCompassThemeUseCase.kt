package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.domain.repository.ThemeRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetSelectedCompassThemeUseCase @Inject constructor(private val themeRepository: ThemeRepository) :
    UseCase<CompassTheme?, Unit>() {
    override fun invoke(param: Unit): Flow<State<CompassTheme?>> {
        return flow {
            emit(State.LoadingState)
            val selectedCompassTheme = themeRepository.getSelectedCompassTheme()
            selectedCompassTheme?.let {
                emit(State.DataState(it))
            } ?: emit(State.DataState(CompassTheme()))
        }
    }
}