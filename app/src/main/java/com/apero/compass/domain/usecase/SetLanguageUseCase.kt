package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.Language
import com.apero.compass.data.entity.Languages
import com.apero.compass.domain.repository.LanguageRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class SetLanguageUseCase @Inject constructor(private val languageRepository: LanguageRepository) :
    UseCase<Boolean, Language>() {
    override fun invoke(param: Language): Flow<State<Boolean>> {
        return flow {
            emit(State.LoadingState)
            try {
                val newLanguages = languageRepository.getLanguages().languages.map {
                    it.selected = it.code.equals(param.code)
                    it
                }
                emit(State.DataState(languageRepository.setLanguages(Languages(newLanguages))))
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }
    }
}