package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.domain.repository.ThemeRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class SetSelectedCompassThemeUseCase @Inject constructor(private val themeRepository: ThemeRepository) :
    UseCase<Boolean, CompassTheme>() {
    override fun invoke(param: CompassTheme) = flow {
        emit(State.LoadingState)
        emit(State.DataState(themeRepository.setSelectedCompassTheme(compassTheme = param)))
    }.flowOn(Dispatchers.IO)
}