package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.domain.repository.ThemeRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class GetSelectedBackgroundThemeUseCase @Inject constructor(private val themeRepository: ThemeRepository) :
    UseCase<BackgroundTheme?, Unit>() {
    override fun invoke(param: Unit): Flow<State<BackgroundTheme?>> {
        return flow {
            emit(State.LoadingState)
            emit(State.DataState(themeRepository.getSelectedBackgroundTheme()))
        }
    }
}