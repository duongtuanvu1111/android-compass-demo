package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.local.FileUtils
import com.apero.compass.data.remote.Api
import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.lang.Exception
import javax.inject.Inject

class DownloadBackgroundThemeUseCase @Inject constructor(
    private val api: Api,
    private val fileUtils: FileUtils
) :
    UseCase<Boolean, BackgroundTheme>() {
    override fun invoke(param: BackgroundTheme): Flow<State<Boolean>> {
        return flow {
            emit(State.LoadingState)
            try {
                val responseBody = api.downloadFile(param.file ?: "")
                val filePath = fileUtils.saveBackground(param, responseBody)
                emit(State.DataState(fileUtils.isValidFile(filePath)))
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }
    }
}