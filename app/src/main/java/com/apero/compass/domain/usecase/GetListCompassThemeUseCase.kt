package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.domain.repository.ThemeRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.*
import java.lang.Exception
import javax.inject.Inject

class GetListCompassThemeUseCase @Inject constructor(private val themeRepository: ThemeRepository) :
    UseCase<List<CompassTheme>, Unit>() {
    override fun invoke(param: Unit): Flow<State<List<CompassTheme>>> {
        val getListCompassFlow = flow {
            try {
                emit(State.DataState(themeRepository.getListCompassTheme()))
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }

        val getSelectedCompassThemeFlow = flow {
            try {
                emit(State.DataState(themeRepository.getSelectedCompassTheme()))
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }

        return getListCompassFlow.zip(getSelectedCompassThemeFlow) { list, selected ->
            if (list is State.DataState && selected is State.DataState) {
                State.DataState(list.data.map {
                    if (selected.data == null) {
                        it.selected = it.id == 0
                    } else {
                        it.selected = it.id == selected.data?.id
                    }
                })
            }
            list
        }.onStart {
            emit(State.LoadingState)
        }

    }
}