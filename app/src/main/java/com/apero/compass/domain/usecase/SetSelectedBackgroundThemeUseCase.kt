package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.domain.repository.ThemeRepository
import com.apero.compass.presentation.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject

class SetSelectedBackgroundThemeUseCase @Inject constructor(private val themeRepository: ThemeRepository) :
    UseCase<Boolean, BackgroundTheme>() {
    override fun invoke(param: BackgroundTheme) = flow {
        emit(State.LoadingState)
        emit(State.DataState(themeRepository.setSelectedBackgroundTheme(param)))
    }.flowOn(Dispatchers.IO)
}