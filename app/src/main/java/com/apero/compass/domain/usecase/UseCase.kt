package com.apero.compass.domain.usecase

import com.apero.compass.presentation.State
import kotlinx.coroutines.flow.Flow


abstract class UseCase<Output, Params>() {
    abstract  operator fun invoke(param: Params): Flow<State<Output>>
}