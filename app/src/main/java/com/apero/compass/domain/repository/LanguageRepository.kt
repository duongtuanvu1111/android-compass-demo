package com.apero.compass.domain.repository

import com.apero.compass.data.entity.Languages

interface LanguageRepository {
    fun getLanguages(): Languages
    fun setLanguages(languages: Languages): Boolean
}