package com.apero.compass.domain.repository

import com.apero.compass.data.entity.BackgroundTheme
import com.apero.compass.data.entity.CompassTheme

interface ThemeRepository {
    suspend fun getListCompassTheme(): List<CompassTheme>
    suspend fun getListBackgroundTheme(): List<BackgroundTheme>
    suspend fun getSelectedCompassTheme(): CompassTheme?
    suspend fun getSelectedBackgroundTheme(): BackgroundTheme?
    suspend fun setSelectedCompassTheme(compassTheme: CompassTheme): Boolean
    suspend fun setSelectedBackgroundTheme(backgroundTheme: BackgroundTheme): Boolean
}