package com.apero.compass.domain.usecase

import com.apero.compass.data.entity.CompassTheme
import com.apero.compass.data.local.FileUtils
import com.apero.compass.data.local.ImageType
import com.apero.compass.data.remote.Api
import com.apero.compass.presentation.State
import com.apero.compass.utils.log.Logger
import kotlinx.coroutines.flow.*
import java.lang.Exception
import javax.inject.Inject

class DownloadCompassThemeUseCase @Inject constructor(
    private val api: Api,
    private val fileUtils: FileUtils
) : UseCase<Boolean, CompassTheme>() {


    override fun invoke(param: CompassTheme): Flow<State<Boolean>> {
        val downloadCircleFlow = flow {
            try {
                val responseBody = api.downloadFile(param.circle ?: "")
                val filePath =
                    fileUtils.saveCompassThumb(param, responseBody, ImageType.COMPASS_CIRCLE)
                emit(State.DataState(fileUtils.isValidFile(filePath)))
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }

        val downloadPlusFlow = flow {
            try {
                val responseBody = api.downloadFile(param.plus ?: "")
                val filePath =
                    fileUtils.saveCompassThumb(param, responseBody, ImageType.COMPASS_PLUS)
                emit(State.DataState(fileUtils.isValidFile(filePath)))
            } catch (e: Exception) {
                emit(State.ErrorState(e))
            }
        }

        val f: Flow<State<Boolean>> =
            downloadCircleFlow.zip(downloadPlusFlow) { circleResult, plusResult ->
                if (circleResult is State.DataState && circleResult.data && plusResult is State.DataState && plusResult.data) {
                    State.DataState(circleResult is State.DataState && circleResult.data && plusResult is State.DataState && plusResult.data)
                }
                circleResult
            }.onStart { emit(State.LoadingState) }


        return f

        /*return downloadCircleFlow.zip(downloadPlusFlow) { circleResult, plusResult ->
            State.DataState(circleResult is State.DataState && circleResult.data && plusResult is State.DataState && plusResult.data)
        }.onStart {
            emit(State.LoadingState)
        }*/

    }
}