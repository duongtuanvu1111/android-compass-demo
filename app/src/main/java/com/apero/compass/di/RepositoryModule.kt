package com.apero.compass.di

import com.apero.compass.data.local.Pref
import com.apero.compass.data.remote.Api
import com.apero.compass.data.repository.LanguageRepositoryImpl
import com.apero.compass.data.repository.ThemeRepositoryImpl
import com.apero.compass.domain.repository.LanguageRepository
import com.apero.compass.domain.repository.ThemeRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideLanguageRepository(pref: Pref): LanguageRepository = LanguageRepositoryImpl(pref)

    @Provides
    @Singleton
    fun provideThemeRepository(api: Api, pref: Pref): ThemeRepository = ThemeRepositoryImpl(api, pref)
}